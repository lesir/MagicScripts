//@ sourceURL=grabqq.js
(function($){
	if(!$ || $.fn){
		$ = document.querySelector;
	}
	var group =  $('#member_search_result_list');
	if(!group){
		console.error('你需要先展开群成员列表');
		return;
	}
	var userList = window.userList = [];
	for (var i = 0; i < group.children.length; i++) {
		var m = group.children[i];
		var qq = m.getAttribute('_uin');
		var nickNameTag = $('#userNick-'+qq);
		var nickName = (nickNameTag && nickNameTag.innerText);
		var obj = {};
		obj[qq] = nickName.replace(/(^\s*)|(\s*$)/g,'');
		userList.push(obj);
	}
	console.log('群名称:'+$('#panelTitle-5').innerText+'\r\n成员:');
	console.log(userList);
	//console.log(JSON.stringify(list));
})($);

