//@ sourceURL=wx_hacker.js
(function(c,b,g) {

    var toUserName = null;//"gh_adf3ed2334be";
    var chatCtrl = WebMM.getCtrlInstants('chat_editor');
    var currentCmd = null;

    var sendCmd = function(cmd) {
    	c.logic("sendMsg").sendText({Msg:{FromUserName:c.model("account").getUserName(), ToUserName:toUserName, Type:1, Content:cmd}}, {onerror:function(a) {
    		"1201" == a && chatCtrl.alert(c.getRes("text_exit_chatroom"));
    	}});
    };

    var showMsgInfoToLinkContainer = function (cmd,msgObj, linkContainer) {
    	var textArea = linkContainer.find('textarea');
    	var str = cmd ? cmd + ":\n" : '';
    	var item = msgObj['Object'].category.item;
    	var itemList = $.isArray(item)?item:[item];
    	for (var i = 0; i < itemList.length; i++) {
    		var article = itemList[i];
    		str += article.title + ': ' + article.url;
    		str += '\n';
    	}
    	str += "\n";

    	var text = textArea.val();
    	text += str;
    	textArea.val(text);
    }

    var recvMsgList = [];
    var msgHandler = function(msg) {
    	if(msg.FromUserName==toUserName){
    		recvMsgList.push({currentCmd:msg});
    		showMsgInfoToLinkContainer(currentCmd,msg,linkContainer);
    		currentCmd = cmdList.shift();
    		if(currentCmd){
    			console.log('正在执行发送：'+currentCmd);
    			sendCmd(currentCmd);
    		}else{
    			console.log('send cmd over!');
    			chatCtrl.alert("消息发送完毕！共接收到"+recvMsgList.length+"条消息。详情见右侧的文本框。");
    		}
    	}
    };

    $.aop.after(c.model('message'),'addMessages',function(msgs){
    	for (var i = msgs.length - 1; i >= 0; i--) {
    		var msg = msgs[i];
    		msgHandler(msg);
    	};
    });

    var makeLinkContainer = function (argument) {
    	var div = $('<div/>').css({
    		position: 'fixed',
    		right: 0,
    		top: 0,
    		width: '50%',
    		height: '90%',
    		'z-index': 0
    	});


    	var btnCss = {
    		position: 'absolute',
    		right: 0,
    		top: '5px'
    	};

    	var remove  = function () {
    		div.remove();
    	}
    	var prev = div.$prev = function (argument) {
    		div.css('z-index', 9999);
    	}
    	var backend = div.$backend = function (argument) {
    		div.css('z-index', 0);
    	}

    	var closeBtn = $('<button/>').text('关闭').css(btnCss).on('click',remove);

    	var prevBtn = $('<button/>').text('前置').css(btnCss).css('right','80px').on('click', prev);

    	var backendBtn = $('<button/>').text('后置').css(btnCss).css('right','40px').on('click', backend);

    	var textArea = $('<textarea/>').css({
    		width: '100%',
    		height: '100%'
    	});

    	div.append(textArea).append(prevBtn).append(backendBtn).append(closeBtn);
    	return div;
    }

    var linkContainer = makeLinkContainer().attr('id',"linkContainer");
    linkContainer.hide().appendTo('body');

    function showContainer () {
    	linkContainer.show();
    	linkContainer.find('textarea').val('');
    	setTimeout(function(){
    		linkContainer.$prev();
    	},25*1000);
    }

    function searchUserName (name) {
    	var userName = name;
    	var contactIns = c.model('contact');
    	if(!contactIns.getContact(name)){
    		var contacts = contactIns.getAllContacts();
    		$.each(contacts,function(key,contact){
    			if(contact.DisplayName==name || contact.NickName==name){
    				userName = key;
    				return false;
    			}
    		})
    	}
    	return userName;
    }

    g.wxHacker = {
    	init:function(userName,sendMsgList) {
    		toUserName = searchUserName(userName);
    		cmdList = sendMsgList;
    		currentCmd = cmdList.shift();
    		recvMsgList = [];
    		showContainer();
    	},
    	grabMessagesOf:function(userName,sendMsgList) {
    		this.init(userName,sendMsgList);
    		if (sendMsgList && sendMsgList.length) {
    			sendCmd(currentCmd, linkContainer);
    		} else {
            // parseAllRespItemToArticles(linkContainer);
        }
    }
}

})(WebMM,jQuery,this);

var getCommandList = function () {
	var list = ['0000'];
	for (var i = 301; i < 371; i++) {
		list.push('' + i);
	}
	return list;
}
var articleCommandList = getCommandList();
wxHacker.grabMessagesOf('鬼脚七',articleCommandList);