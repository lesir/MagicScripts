//@ sourceURL=grabTianYaBook.js
var jqUrl = 'http://code.jquery.com/jquery.min.js',
bookUrl = '';
function getScript(url, success) {
    var script = document.createElement('script');
    script.src = url;
    var head = document.getElementsByTagName('head')[0], done = false;
    script.onload = script.onreadystatechange = function () {
        if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {
            done = true;
            success();
            script.onload = script.onreadystatechange = null;
            head.removeChild(script);
        }
    };
    head.appendChild(script);
}
function grabBook () {
    var charpters = [];
    var getCharpterContent = function (url) {
        $.ajax({
           url:url,
           success:function(resp){
               var doc = $(resp);
               var main = doc.filter('#main');
               var title = $.trim(main.find('h1').text());
               var content =$.trim(main.find('p[align=center]').next("p").text());
               charpters.push(title+'\n');
               charpters.push(content);
           },
           async:false
       });
    };
    var printCharpters = function (content) {
        $('<textarea/>').css({position:'fixed',top:0,right:0,width:'50%',height:'100%'}).val(content).appendTo('body');
    }
    $('.book dl dd a[href]').each(function(_,link){
       var url = link.href;
       console.log('loading:'+url);
       getCharpterContent(url);
       printCharpters(charpters.join('\n\n\n'));
   });
}
if(typeof jQuery === 'undefined'){
    getScript(jqUrl,grabBook);    
}else{
    grabBook();
}
